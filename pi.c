#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char* argv[]) {
    int k;
    double sum = 0.0;
    double factor = 1.0;
    int n;
    int thread_count;

    thread_count = strtol(argv[1], NULL, 10);

    printf("Enter n\n");
    scanf("%d", &n);
# pragma omp parallel for num_threads(thread_count)
    for (k = 0; k < n; ++k) {
        sum += factor/(2*k+1);
        factor *= -1;
    }

    double pi_approx = 4.0 * sum;
    printf("PI is = %f\n", pi_approx);
}

